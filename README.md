# Hörner, J., Voigt, A., Braun, C. (2022). Snowball Earth initiation and the thermodynamics of sea ice. Journal of Advances in Modeling Earth Systems.

Code repository for ICON runscripts and python post processing scripts used in the analysis.


The associated data for the analysis is available via https://doi.org/10.25365/phaidra.304.
